# README #

This project aims at creating an Apache Spark Cluster, with one Master, and two slaves.

### What is this repository for? ###

* Apache Spark Cluster
* 1.0.0
* A good guide can be found [here](https://blog.knoldus.com/2015/04/14/setup-a-apache-spark-cluster-in-your-single-standalone-machine/)

### How do I get set up? ###

* On OSX edit the /etc/sudoers file and add the following content
`
Cmnd_Alias VAGRANT_EXPORTS_ADD = /usr/bin/tee -a /etc/exports
Cmnd_Alias VAGRANT_NFSD = /sbin/nfsd restart
Cmnd_Alias VAGRANT_EXPORTS_REMOVE = /usr/bin/sed -E -e /*/ d -ibak /etc/exports
%admin ALL=(root) NOPASSWD: VAGRANT_EXPORTS_ADD, VAGRANT_NFSD, VAGRANT_EXPORTS_REMOVE
`
* Run vagrant up from the vagrant folder

### Who do I talk to? ###

* [Patrique Legault](mailto:patrique.legault@gmail.com)