#bin/bash

# Install the ssh clients and server
sudo yum install openssh-client -y
sudo yum install openssh-server -y

# Copy the Slaves folder to the Spark folder
sudo cp /vagrant/assets/slave1.spark-env.sh $SPARK_HOME/conf/spark-env.sh

# Copy the private key to the server
cp /vagrant/files/ssh/private_key /home/vagrant/.ssh/id_rsa
chmod 400 /home/vagrant/.ssh/id_rsa
sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa