#bin/bash

# Add the necessary host entriesto the /etc/hosts folder
sudo cp /vagrant/assets/hosts /etc/hosts

# Install wget on the machine since we need it to install Java
sudo yum install wget -y

# Perform an update
sudo yum update -y

# Install Java 8
cd /opt
sudo wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.rpm"
sudo yum localinstall jdk-8u131-linux-x64.rpm -y
sudo rm -f jdk-8u131-linux-x64.rpm
echo java -v

# Install Scala
wget http://downloads.lightbend.com/scala/2.11.8/scala-2.11.8.rpm
sudo yum install scala-2.11.8.rpm -y
sudo rm -f scala-2.11.8.rpm
echo scala -v

# Install Apache Spark
cd /home/vagrant
mkdir Spark
cd Spark
sudo wget https://d3kbcqa49mib13.cloudfront.net/spark-2.2.0-bin-hadoop2.7.tgz
sudo tar -xvzf spark-2.2.0-bin-hadoop2.7.tgz
sudo rm -f spark-2.2.0-bin-hadoop2.7.tgz

# Changing the permissions on the folder to run as vagrant
cd ../
sudo chown -R vagrant:vagrant /home/vagrant/Spark/

cd /home/vagrant
sudo cp /vagrant/assets/.bashrc .
source  .bashrc
echo $JAVA_HOME
echo $SPARK_HOME