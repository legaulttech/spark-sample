#bin/bash

# Install the ssh clients and server
sudo yum install openssh-client -y
sudo yum install openssh-server -y

# Copy the Slaves folder to the Spark folder
sudo cp /vagrant/assets/slaves $SPARK_HOME/conf/slaves
sudo cp /vagrant/assets/master.spark-env.sh $SPARK_HOME/conf/spark-env.sh

# Copy the private key to the server
cp /vagrant/files/ssh/private_key /home/vagrant/.ssh/id_rsa
chmod 400 /home/vagrant/.ssh/id_rsa
sudo chown vagrant:vagrant /home/vagrant/.ssh/id_rsa

# Start the server as the Vagrant user
su -c "cd /home/vagrant/Spark/spark-2.2.0-bin-hadoop2.7/sbin && ./start-all.sh" -s /bin/sh vagrant

# Run an example command on the master
su -c "cd /home/vagrant/Spark/spark-2.2.0-bin-hadoop2.7/bin && ./spark-shell --driver-memory 1g -i /vagrant/examples/pi.scala" -s /bin/sh vagrant